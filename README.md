# MasterMDR - Braziliex Bitcoin
====================

### Instalação

- Execute **`composer require brunomluiz/mdrbraziliex`**

ou adicione o repositório em seu **composer.json**:

```
#!json
"require": {
	...
	"brunomluiz/mdrbraziliex": '1.*'
	...
}
```

### Laravel

1. Registre o provider em **`/config/app.php`**.

```
#!php
... 
Pagamentos\Braziliex\Providers\BraziliexServiceProvider::class,
...	
```

2. Cadastre o método de pagamento com o nome de **`Braziliex`**.
 
3. Copiar a chave **`"key"`** em: https://braziliex.com/exchange/gateway_plugin.php e cadastrar no método de pagamento no campo **`"Token comum"`**.

4. Altere o arquivo: **"/resources/views/default/pedidos/visualizar.blade.php"** e adicione o código abaixo no foreach.

```
#!php
... 
@elseif($metodoPagto->name == "Braziliex" && config('braziliex.key')){{--BRAZILIEX - bitcoin--}}
    <div class="form-group col-md-6">
        <a href="{{ route('pagamento.braziliex', [$dados->id]) }}" class="btn btn-primary">{{ $metodoPagto->name }} - Bitcoin <i class="glyphicon glyphicon-bitcoin"></i></a>
    </div>
@endif
... 
```

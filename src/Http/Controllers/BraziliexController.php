<?php

/*
 * Esse arquivo faz parte de <MasterMundi/Master MDR>
 * (c) Bruno Martins brunoluiz[at]gmail.com
 *
 */

namespace Pagamentos\Braziliex\Http\Controllers;

use Log;
use App\Models\Sistema;
use App\Models\Pedidos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pagamentos\Braziliex\Services\BraziliexService;

/**
 * Class BraziliexController.
 */
class BraziliexController extends Controller
{
    /**
     * @var
     */
    protected $sistema;

    /**
     * BraziliexController constructor.
     */
    public function __construct()
    {
        $this->sistema = Sistema::find(1);
    }

    /**
     * @param Pedidos $pedido
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function index(Pedidos $pedido)
    {
        if ($pedido->dadosPagamento->metodoPagamento === null) {
            $pedido->dadosPagamento->metodo_pagamento_id = \Config::get('braziliex.id');
            $pedido->dadosPagamento->status = 4;
            $pedido->dadosPagamento->save();
            $pedido->dadosPagamento->load('metodoPagamento');
        }

        return view('braziliex::index', [
            'sistema' => $this->sistema,
            'title' => 'Pagamento pedido #'.$pedido->id,
            'pedido' => $pedido,
            'taxas' => [
                'valor' => $pedido->dadosPagamento->metodoPagamento->taxa_valor,
                'porcentagem' => $pedido->dadosPagamento->metodoPagamento->taxa_porcentagem,
                'descricao' => $pedido->dadosPagamento->metodoPagamento->taxa_descricao,
            ],
            'valores' => [
                'valor' => $pedido->valor_total + ($pedido->dadosPagamento->metodoPagamento->taxa_valor),
                'porcentagem' => $pedido->valor_total * ($pedido->dadosPagamento->metodoPagamento->taxa_porcentagem / 100),
                'pedido_total' => $pedido->valor_total + ($pedido->dadosPagamento->metodoPagamento->taxa_valor) + ($pedido->valor_total * ($pedido->dadosPagamento->metodoPagamento->taxa_porcentagem / 100)),
            ],
        ]);
    }

    /**
     * @param Pedidos $pedido
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addr(Pedidos $pedido, Request $request)
    {
        if (BraziliexService::setAddress($pedido, $request->get('address'))) {
            return response()->json([], 200);
        }

        return response()->json([], 500);
    }

    /**
     * @param Pedidos $pedido
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function callback(Pedidos $pedido, Request $request)
    {
        if (BraziliexService::checkAddress($pedido)) {
            return response()->json(['payment' => true], 200);
        }

        return response()->json(['payment' => false], 500);
    }

    /**
     * @param Pedidos $pedido
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel(Pedidos $pedido)
    {
        try {
            $pedido->status = 1;
            $pedido->save();
            $pedido->dadosPagamento->status = 0;
            $pedido->dadosPagamento->save();
            Log::info("PEDIDO #{$pedido->id} - CANCELADO PELO USU�RIO");

            return response()->json([], 200);
        } catch (\Exception $e) {
            return response()->json([], 500);
        }
    }
}

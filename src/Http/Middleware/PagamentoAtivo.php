<?php

/*
 * Esse arquivo faz parte de <MasterMundi/Master MDR>
 * (c) Bruno Martins brunoluiz[at]gmail.com
 *
 */

namespace Pagamentos\Braziliex\Http\Middleware;

use Closure;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class PagamentoAtivo.
 */
class PagamentoAtivo
{
    /**
     * @param $request
     * @param Closure $next
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            if (! \Config::get('braziliex.key')) {
                throw new ModelNotFoundException();
            }

            return $next($request);
        } catch (ModelNotFoundException $e) {
            return redirect()->route('home');
        }
    }
}

<?php

/*
 * Esse arquivo faz parte de <MasterMundi/Master MDR>
 * (c) Bruno Martins brunoluiz[at]gmail.com
 *
 */

Route::get('pagamento/braziliex/{braziliex_pedido}', [
    'middleware' => ['auth', 'pagamento'],
    'as' => 'pagamento.braziliex',
    'uses' => 'BraziliexController@index',
]);

Route::get('pagamento/braziliex/{braziliex_pedido}/callback', [
    'middleware' => ['auth', 'pagamento'],
    'as' => 'pagamento.braziliex.callback',
    'uses' => 'BraziliexController@callback',
]);

Route::post('pagamento/braziliex/{braziliex_pedido}/addr', [
    'middleware' => ['auth', 'pagamento'],
    'as' => 'pagamento.braziliex.addr',
    'uses' => 'BraziliexController@addr',
]);

Route::post('pagamento/braziliex/{braziliex_pedido}/cancel', [
    'middleware' => ['auth', 'pagamento'],
    'as' => 'pagamento.braziliex.cancel',
    'uses' => 'BraziliexController@cancel',
]);

@extends('default.layout.main')

@section('content')

    @include('default.errors.errors')

    <section class="content-header">
        <h1>
            Braziliex - Bitcoin
        </h1>

        <ol class="breadcrumb hidden-xs">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Braziliex - Bitcoin</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">

                <div class="col-md-6">
                    <div class="receive_crypto"
                         key="{{ config('braziliex.key') }}"
                         value_brl="{{ $valores['pedido_total'] }}"
                         id_order="{{ $pedido->id }}"
                         description="Pedido#{{ $pedido->id }}">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box">
                        <div class="box-body">
                            <p class="lead">Montante</p>
                            <div class="table-responsive">
                                <table class="table" style="margin-bottom: 0;">
                                    <tbody>
                                        @if($taxas['valor'] > 0 || $taxas['porcentagem'] > 0)
                                            <tr>
                                                <th style="width:50%">Subtotal</th>
                                                <td>{{ $sistema->moeda }} {{ number_format($pedido->valor_total, 2, ',', '.') }}</td>
                                            </tr>
                                            <tr>
                                                <th>{{ empty($taxas['descricao']) ? 'Taxa' : $taxas['descricao'] }}</th>
                                                <td>{{ $sistema->moeda }} {{ number_format($taxas['valor'] + $valores['porcentagem'], 2, ',', '.') }}</td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <th>Total</th>
                                            <td>{{ $sistema->moeda }} {{ number_format($valores['pedido_total'], 2, ',', '.') }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <hr>
                            <h3 style="margin-top: 0;"><i class="fa fa-warning"></i> Atenção</h3>
                            <p>Ao <b>"Ver código QR"</b> e fazer o pagamento, aguarde a confirmação do mesmo.</p>
                            <p>Assim que seu endereço for confirmado na <b>block-chain</b>, seu depósito será confirmado automaticamente e um aviso emitido.</p>
                            <a href="{{ redirect()->back()->getTargetUrl() }}" class="btn btn-primary mdr_voltar"><i class="fa fa-mail-reply"></i> Voltar</a>
                            <a href="javascript:;" class="btn btn-danger mdr_cancelar" style="display: none;"><i class="fa fa-remove"></i> Cancelar depósito</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
        </div>
    </section>
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert/sweetalert2.min.css') }}">
@endsection

@section('script')
    <script src="{{ asset('plugins/sweetalert/sweetalert2.js') }}" type="text/javascript"></script>
    <script src="https://braziliex.com/js/gateway_embed.js" type="text/javascript"></script>
    <script>
        (function ($) {
            $.each(['show', 'hide'], function (i, ev) {
                var el = $.fn[ev];
                $.fn[ev] = function () {
                    this.trigger(ev);
                    return el.apply(this, arguments);
                };
            });
        })(jQuery);

        $(document).ready(function (){
            $('.mdr_cancelar').click(function(e){
                e.preventDefault();
                Swal.fire({
                    title: 'Aguarde...',
                    html: 'Estamos processando sua solicitação.',
                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading();
                    },
                    onOpen: () => {
                        $.ajax({
                            url: '{{ route('pagamento.braziliex.cancel', [ $pedido->id ]) }}',
                            type: 'POST',
                            data: {
                                _token: "{{ csrf_token() }}",
                            },
                            success: function (response) {
                                Swal.fire({
                                    title: 'Atenção',
                                    text: 'Seu depósito foi cancelado!',
                                    type: 'success'
                                }).then((result) => {
                                    if (result.value || result.dismiss) {
                                        window.location.href = '{{ route('depositos.aguardando.deposito', [Auth::user()->id]) }}';
                                    }
                                });
                            },
                            error: function () {
                                Swal.close();
                                Swal.fire({
                                    title: 'Atenção',
                                    text: "Não conseguimos cancelar seu depósito. Tente novamente.",
                                    type: 'error',
                                }).then((result) => {
                                    if (result.value || result.dismiss) {
                                        window.location.reload(true);
                                    }
                                });
                            }
                        });
                    }
                });
            });

            $('#fase_10').on({
                show: function () {
                    $('.mdr_voltar').hide();
                    $('.mdr_cancelar').show();
                    $.ajax({
                        url: '{{ route('pagamento.braziliex.addr', [ $pedido->id ]) }}',
                        type: 'POST',
                        data: {
                            _token: "{{ csrf_token() }}",
                            address: $(document).find('#rc_addr').text(),
                        },
                        success: function (response) {
                        },
                        error: function () {
                            Swal.close();
                            Swal.fire({
                                title: 'Atenção',
                                text: "Não conseguimos confirmar seu pagamento. Atualize a página e tente novamente!",
                                type: 'error',
                            }).then((result) => {
                                if (result.value || result.dismiss) {
                                    window.location.reload(true);
                                }
                            });
                        }
                    });
                }
            });
            $('#fase_50').on({
                show: function () {
                    Swal.fire({
                        title: 'Aguarde...',
                        html: 'Estamos processando seu pagamento',
                        allowOutsideClick: false,
                        onBeforeOpen: () => {
                            Swal.showLoading();
                        },
                        onOpen: () => {
                            $.ajax({
                                url: '{{ route('pagamento.braziliex.callback', [ $pedido->id ]) }}',
                                type: 'GET',
                                data: {
                                    address: $(document).find('#rc_addr').text(),
                                },
                                success: function (response) {
                                    Swal.fire({
                                        title: 'Atenção',
                                        text: 'Seu pagamento foi confirmado e já depositamos o saldo em sua conta!',
                                        type: 'success'
                                    }).then((result) => {
                                        if (result.value || result.dismiss) {
                                            window.location.href = '{{ route('depositos.aguardando.deposito', [Auth::user()->id]) }}';
                                        }
                                    });
                                },
                                error: function () {
                                    Swal.close();
                                    Swal.fire({
                                        title: 'Atenção',
                                        text: "Não conseguimos confirmar seu pagamento. Se o pagamento foi efetuado entre em contato conosco.",
                                        type: 'error',
                                    }).then((result) => {
                                        if (result.value || result.dismiss) {
                                            window.location.reload(true);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        });
    </script>
@endsection
<?php

/*
 * Esse arquivo faz parte de <MasterMundi/Master MDR>
 * (c) Bruno Martins brunoluiz[at]gmail.com
 *
 */

namespace Pagamentos\Braziliex\Providers;

use App\Models\Pedidos;
use App\Models\MetodoPagamento;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Illuminate\Console\Scheduling\Schedule;
use Pagamentos\Braziliex\Services\BraziliexService;

/**
 * Class BraziliexServiceProvider.
 */
class BraziliexServiceProvider extends ServiceProvider
{
    public function boot()
    {
        /*
         * Registrando config
         */
        $this->registerConfig();

        /*
         * Registrando middleware
         */
        $this->registerMiddleware();

        /*
         * Registrando rotas
         */
        $this->registerRouter();

        /*
         * Injetando modelos por parametro
         */
        $this->registerModel();

        /*
         * Injetando views
         */
        $this->loadViewsFrom(dirname(__FILE__, 2).'/Resources/Views', 'braziliex');

        /*
         * Shedule
         */
        $this->app->booted(function () {
            $schedule = $this->app->make(Schedule::class);
            $schedule->call(function () {
                \Log::info('####### BRAZILIEX SHEDULE - START ##########');
                BraziliexService::checkAddresses();
                \Log::info('####### BRAZILIEX SHEDULE - END ##########');
            })->everyFiveMinutes();
        });
    }

    public function register()
    {
        // TODO: Implement register() method.
    }

    public function registerModel()
    {
        $this->app['router']->bind('braziliex_pedido', function ($value) {
            try {
                return Pedidos::where('status', 1)->where('user_id', Auth::user()->id)->findOrFail($value);
            } catch (\Exception $e) {
                return redirect()->route('home')->send();
            }
        });
    }

    public function registerMiddleware()
    {
        $this->app['router']->middleware('pagamento', \Pagamentos\Braziliex\Http\Middleware\PagamentoAtivo::class);
    }

    public function registerConfig()
    {
        /*
         * Verificar se o metodo de pagamento esta ativo
         */
        try {
            $pagamento = MetodoPagamento::where('name', '=', 'Braziliex')->where('status', '=', 1)->firstOrFail();

            if (! $pagamento->configuracao[0]) {
                throw new \Exception();
            }

            Config::set('braziliex', [
                'id' => $pagamento->id,
                'key' => $pagamento->configuracao[0],
            ]);
        } catch (\Exception $e) {
            return;
        }
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function registerRouter()
    {
        $this->app['router']->group([
            'namespace' => 'Pagamentos\Braziliex\Http\Controllers',
        ], function () {
            require dirname(__FILE__, 2).'/Http/routes.php';
        });
    }
}

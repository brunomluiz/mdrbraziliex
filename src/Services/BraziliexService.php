<?php

/*
 * Esse arquivo faz parte de <MasterMundi/Master MDR>
 * (c) Bruno Martins brunoluiz[at]gmail.com
 *
 */

namespace Pagamentos\Braziliex\Services;

use Log;
use GuzzleHttp\Client;
use App\Models\Pedidos;
use App\Services\Pagamentos;
use Carbon\Carbon as Carbon;
use Illuminate\Support\Facades\DB as DB;
use App\Models\DadosPagamento as DadosPagamento;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Class BraziliexService.
 */
abstract class BraziliexService
{
    /**
     * @param Pedidos $pedido
     * @return bool
     */
    public static function checkAddress(Pedidos $pedido)
    {
        $pedido->load('user', 'dadosPagamento');

        $client = new Client();
        $res = $client->get(
            'https://braziliex.com/api/v1/gateway/pay/confirm/'.\Config::get('braziliex.key').'/'.$pedido->dadosPagamento->invoice_id
        );

        if ($res->getStatusCode() === 200) {
            $data = json_decode($res->getBody());

            if ($data && $data->success = 1 && $data->status == 'completed_total') {
                self::pay($pedido);

                return true;
            }

            Log::info("PEDIDO #{$pedido->id} - STATUS: {$data->status}");

            //VERIFICAR SE O PEDIDO PODE SER CANCELADO
            if (Carbon::now()->gte(Carbon::parse($pedido->created_at->addHours(1)))) {
                $pedido->status = 3;
                $pedido->save();
                $pedido->dadosPagamento->status = 3;
                $pedido->dadosPagamento->save();
                Log::info("PEDIDO #{$pedido->id} - PRAZO PARA PAGAMENTO ATINGIDO!");

                return true;
            }

            return false;
        }

        Log::info("ERRO: PEDIDO #{$pedido->id} - BRAZILIEX NOT FOUND");

        return true;
    }

    public static function checkAddresses()
    {
        $pedidos = Pedidos::with('itens.itens', 'dadosPagamento', 'usuario')
            ->whereHas('dadosPagamento', function ($query) {
                $query->whereNotNull('invoice_id')
                    ->where('invoice_id', '<>', '')
                    ->where('status', 4)
                    ->where('metodo_pagamento_id', \Config::get('braziliex.id'));
            })
            ->whereIn('status', [1, 4])->get();

        foreach ($pedidos as $pedido) {
            self::checkAddress($pedido);
        }
    }

    /**
     * @param Pedidos $pedido
     * @param $address
     * @return bool
     */
    public static function setAddress(Pedidos $pedido, $address)
    {
        try {
            $pedido->dadosPagamento->metodo_pagamento_id = \Config::get('braziliex.id');
            $pedido->dadosPagamento->status = 4;
            $pedido->dadosPagamento->invoice_id = $address;
            $pedido->dadosPagamento->save();

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param Pedidos $pedido
     * @return bool
     */
    private static function pay(Pedidos $pedido)
    {
        $dadosPagamento = DadosPagamento::where('pedido_id', $pedido->id)->first();

        Log::info('####### PAGAMENTO BRAZILIEX ##########');
        Log::info('DEPÓSITO: '.$pedido->id);
        Log::info('HASH: '.$dadosPagamento->invoice_id);

        DB::beginTransaction();

        try {
            $dadosPagamento->documento = 'Confirmação automática Braziliex via sistema';
            $dadosPagamento->status = 2;
            $dadosPagamento->data_pagamento = Carbon::now();
            $dadosPagamento->data_pagamento_efetivo = Carbon::now();
            $dadosPagamento->responsavel_user_id = 1;
            $dadosPagamento->metodo_pagamento_id = \Config::get('braziliex.id');
            $dadosPagamento->invoice_id = $dadosPagamento->invoice_id;
            $dadosPagamento->save();

            $pedido->status = 2;
            $pedido->save();

            $pagamento = new Pagamentos($pedido);

            $erros = $pagamento->efetivarPagamento();

            if ($erros > 0) {
                DB::rollback();
                Log::info('Houve erros ao processar a confirmação de pagamento BRAZILIEX do depósito #'.$pedido->id);
                Log::info('');

                return false;
            } else {
                DB::commit();
                Log::info('Depósito #'.$pedido->id.' confirmado com sucesso');
                Log::info('');

                return true;
            }
        } catch (ModelNotFoundException $e) {
            DB::rollback();
            Log::info('Houve erros ao processar a confirmação de pagamento BRAZILIEX do depósito #'.$pedido->id);
            Log::info('');

            return false;
        }
    }
}
